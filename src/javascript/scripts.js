var $ = require('jquery');
var Rellax = require('./modules/para.js');
var slick = require('./vendor/slick');
var body = document.querySelector('body');

if (document.querySelector('.header--mobile-button')) {
  document.querySelector('.header--mobile-button').addEventListener('click', function() {
    if (body) {
      if (body.classList.contains('nav-is-open')) {
        body.classList.remove('nav-is-open');
      } else {
        body.classList.add('nav-is-open');
      }
    }
  })
}

if (document.querySelectorAll('.rellax').length > 0) {
  var rellax = new Rellax('.rellax');
}

var isInViewport = function(elem) {
  var bounding = elem.getBoundingClientRect();
  return (
    bounding.top >= 0 &&
    bounding.left >= 0 &&
    bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

if (document.querySelectorAll('.inview').length > 0) {

  window.addEventListener('scroll', function(event) {
    var elems = document.querySelectorAll('.inview');
    for (i = 0; i < elems.length; i++) {
      var elem = elems[i];
      if (isInViewport(elem)) {
        elem.classList.add('is-inview');
      }
    }

  }, false);
}



$('#patientLoginForm').submit(function(e) {
  var self = this;
  e.preventDefault();
  var data = $(this).serialize();
  var $errorContainer = $(this).find('.is-error');

  var handleError = function() {
    $(self).removeClass('is-disabled');
    $errorContainer.text("Something went wrong. Please try again");
  }

  var handleSuccess = function() {
    $(self).removeClass('is-disabled');
    window.location.href = window.location.href;
  }


  $(self).addClass('is-disabled');
  $errorContainer.empty();
  $.ajax({
    method: 'POST',
    url: '/',
    data: data,
    dataType: 'json',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(data) {
      if (data && data.success) {
        handleSuccess(data);
      } else {
        handleError();
      }
    },
    error: function(data) {
      handleError();
    }
  });

});



$('#registerForm').submit(function(e) {
  var self = this;
  e.preventDefault();
  var formdata = $(this).serializeArray();
  var data = {};
  $(formdata).each(function(index, obj) {
    data[obj.name] = obj.value;
  });
  var $errorContainer = $(this).find('.is-error');

  var handleError = function() {
    $(self).removeClass('is-disabled');
    $errorContainer.text("Something went wrong. Please try again");
  }

  var handleSuccess = function() {
    $(self).removeClass('is-disabled');
    window.location.href = window.location.href;
  }


  $(self).addClass('is-disabled');
  $errorContainer.empty();
  console.log("Sending data", data);
  $.ajax({
    method: 'POST',
    url: window.location.href,
    data: data,
    dataType: 'json',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(data) {
      // console.log("Success", data);
      // return;
      if (data && data.success) {
        handleSuccess(data);
      } else {
        handleError();
      }
    },
    error: function(data) {
      // console.log("Error", data);
      // return;
      handleError();
    }
  });

});


$('.is-sign-in').on('click', function() {
  $('.modal--wrap').addClass('is-open');
  return false;
})

$('.faqs--title, .references--title').on('click', function() {
  $(this).parent().toggleClass('is-open');
})

$('.cbt--module').on('click', function() {
  var $target = $(".js-cbt-target");
  var title = $(this).find('h4').html();
  var description = $(this).find('p').text();
  var titleComponents = title.split("<br>");
  $target.find('h2').text(titleComponents[0] + ": " + titleComponents[1]);
  $target.find('p').text(description);

  $('.cbt--modules .is-active').removeClass('is-active');
  $(this).addClass('is-active');

  $('html, body').animate({
    scrollTop: $target.offset().top - 80
  });

  $('.cbt--iframe').attr('src', $('.cbt--iframe').attr('src'));

  return false;
})


$(".carousel").slick({

  // centerPadding: '75px',
  slidesToShow: 1,
  draggable: true,
  infinite: true,
  dots: true

});