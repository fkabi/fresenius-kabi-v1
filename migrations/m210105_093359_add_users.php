<?php

namespace craft\contentmigrations;

use Craft;
use craft\db\Migration;

use craft\elements\User;

/**
 * m210105_093359_add_users migration.
 */
class m210105_093359_add_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $users = [
            ['george', 'george@agency.com'],
            ['barry', 'barry@agency.com'],
            ['jean', 'jean@agency.com'],
        ];

        foreach ($users as $userData) {
            [$username, $email] = $userData;

            $this->createUser($username, $email);
        }
    }

    protected function createUser($username, $email)
    {
        $user = new User;

        $user->username = $username;
        $user->email = $email;
        $user->admin = true;

        $user->newPassword = CRAFT_ENVIRONMENT === 'dev'
            ? $email
            : md5(uniqid().rand());

        $user->passwordResetRequired = CRAFT_ENVIRONMENT !== 'dev';

        Craft::$app->getElements()->saveElement($user, false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m210105_093359_add_users cannot be reverted.\n";
        return false;
    }
}
