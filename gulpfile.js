var gulp = require('gulp');

var browserSync = require('browser-sync').create();


// Gulp plugins
var nodemon = require('gulp-nodemon'),
    preprocess = require('gulp-preprocess'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    gulpFilter = require('gulp-filter'),
    plumber = require('gulp-plumber'),

    htmlmin = require('gulp-htmlmin'),

    livereload = require('gulp-livereload'),
    rename = require('gulp-rename'),
    browserify = require('browserify'),
    browserifyHandlebars = require('browserify-handlebars'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    es = require('event-stream'),
    source = require('vinyl-source-stream'),
    autoprefixer = require('gulp-autoprefixer');


var ENVIRONMENT = 'local';

var ENV = {
    LOCAL: 'local',
    PRODUCTION: 'production',
};



function errorLog(err) {
    console.log(err);
    console.log(err.stack);
}

// Gulp paths
var paths = {
    frontend: {
        dist: {
            base: './web/',
            local: './web/',
            production: './web/',
            css: 'css/',
            js: 'js/',

        },
        styles: 'src/sass/*.scss',
        allStyles: 'src/sass/**/*.scss',
        scripts: 'src/javascript/scripts.js',
        allScripts: 'src/javascript/**/*.*'
    }
};


gulp.task('sass', function() {
    // console.log("YEAH", gulp.dest(paths.dist.css));
    var localConfig = {
            outputStyle: "nested",
            debugInfo: true,
            errLogToConsole: true,
            includePaths: ['./node_modules/susy/sass']
        },
        distConfig = {
            outputStyle: "compressed",
            errLogToConsole: true,
            includePaths: ['./node_modules/susy/sass']
        },
        renameConfig = {
            suffix: '.min' //'.min'
        };


    gulp.src(paths.frontend.styles)
        .pipe(plumber())
        .pipe(sass(ENVIRONMENT !== ENV.LOCAL ? distConfig : localConfig))
        .pipe(rename(renameConfig))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'ie >= 9'],
            cascade: false
        }))
        .pipe(gulp.dest(paths.frontend.dist[ENVIRONMENT] + paths.frontend.dist.css))
        .pipe(browserSync.stream());


});


gulp.task('browserify-frontend', function() {

    var mainFrontendFilePath = './' + paths.frontend.scripts, // path has to be relative
        renameConfig = {
            suffix: '.min'
        },
        isLocal = ENVIRONMENT === ENV.LOCAL;

    // console.log("About to browserify");
    browserify({
            entries: [mainFrontendFilePath],
            paths: [],
            debug: isLocal
        })
        .transform(browserifyHandlebars)
        .bundle() // -- Turning off debug because it slows down local dev and it just annoying...
        .on('error', errorLog)
        .pipe(source('scripts.js'))
        .pipe(buffer())
        .pipe(!isLocal ? uglify().on('error', gutil.log) : gutil.noop())
        //.pipe(!isLocal ? rename(renameConfig) : gutil.noop())
        .pipe(gulp.dest(paths.frontend.dist[ENVIRONMENT] + paths.frontend.dist.js))
        .pipe(browserSync.stream());

});


gulp.task('start-browserify', function() {
    if (ENVIRONMENT === ENV.LOCAL) {
        gulp.watch(paths.frontend.allScripts, ['browserify-frontend']);
        // gulp.watch(paths.templates, ['browserify']);
    }
    gulp.start('browserify-frontend');

    // if (ENVIRONMENT === ENV.LOCAL) {
    //     gulp.watch(paths.backend.allScripts, ['browserify-backend']);
    //     // gulp.watch(paths.templates, ['browserify']);
    // }
    // gulp.start('browserify-backend');
})


var build = function() {


    gulp.start('sass');
    gulp.start('start-browserify');

    if (ENVIRONMENT == ENV.LOCAL) {
        gulp.watch(paths.frontend.allStyles, ['sass']);
    }

    if (ENVIRONMENT == ENV.LOCAL) {
        browserSync.init({

            proxy: "kabicare.localdev" // makes a proxy for localhost:8080
        });
    }


}


// Default Task
gulp.task('default', function() {
    ENVIRONMENT = ENV.LOCAL;
    build();
});

// Default Task
gulp.task('build', function() {
    ENVIRONMENT = ENV.PRODUCTION;
    build();
});