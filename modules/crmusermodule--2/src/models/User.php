<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\models;



use Craft;
use craft\base\Model;
use craft\web\User as WebUser;
use yii\web\IdentityInterface;
use Yii;

/**
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
class User extends WebUser
{
  public $identityClass = 'modules\crmusermodule\elements\User';

   // public function generateToken(int $userId)
   //  {
   //      $token = Craft::$app->getSecurity()->generateRandomString(100);
   //      Craft::$app->getSession()->set($this->tokenParam, $token);
   //  }

    public function login(IdentityInterface $identity, $duration = 0)
    {
        if ($this->beforeLogin($identity, false, $duration)) {
            
            $this->switchIdentity($identity, $duration);
            $id = $identity->getId();
            $ip = Yii::$app->getRequest()->getUserIP();
            if ($this->enableSession) {
           
                $log = "User '$id' logged in from $ip with duration $duration.";
            } else {
                $log = "User '$id' logged in from $ip. Session not enabled.";
            }

            $this->regenerateCsrfToken();

            Yii::info($log, __METHOD__);
            $this->afterLogin($identity, false, $duration);
        }
       
        return !$this->getIsGuest();
    }

    public function logout($destroySession = true)
    {
        $session = Craft::$app->getSession();
        $session->destroy ();

        return parent::logout ($destroySession);
    }



   // 'identityClass' => 'app\models\User'
}
