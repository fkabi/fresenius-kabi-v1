<?php
/**
 * CRM User Module module for Craft CMS 3.x
 *
 * My mod
 *
 * @link      http://lowpitch.com
 * @copyright Copyright (c) 2019 Me
 */

namespace modules\crmusermodule\twigextensions;

use modules\crmusermodule\CrmUserModule;

use Craft;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Me
 * @package   CrmUserModule
 * @since     1
 */
class CrmUserModuleTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'CrmUserModule';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            // new \Twig_SimpleFilter('someFilter', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
    * @return array
     */
    public function getFunctions()
    {

        return [
            // new \Twig_SimpleFunction('someFunction', [$this, 'someInternalFunction']),
        ];
    }

   public function getTokenParsers () {

        return [
            new RequireHCPTokenParser(),
            new RequirePatientTokenParser(),
        ];
   }

   public function getGlobals() {
        
        $globals = [];
        $session = Craft::$app->getSession();
        $hcpValue = $session->get('hcp_status');
        if ($hcpValue) {
            $globals['hcpUser'] = true;
        } else {
            $globals['hcpUser'] = false;
        }
        return $globals;
   }
}
