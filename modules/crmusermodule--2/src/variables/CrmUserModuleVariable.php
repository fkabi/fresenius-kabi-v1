<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\variables;

use modules\crmusermodule\CrmUserModule;

use Craft;

/**
 * CrmUser Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.crmUserModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
class CrmUserModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.crmUserModule.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.crmUserModule.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
