<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\services;

use modules\crmusermodule\CrmUserModule;
use craft\services\Users as BaseUsers;
use Craft;
use craft\base\Component;
use infusionsoft\InfusionsoftException;
use Infusionsoft\Http\HttpException;
use Infusionsoft\Http\TokenExpiredException;


/**
 * Infusionsoft Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
class Infusionsoft extends Component
{

    public $config;
   
    private $_client;

    protected function getClient () {
        if ($this->_client) {
            return $this->_client;
        }

        // output: /myproject/index.php
        $currentPath = $_SERVER['PHP_SELF']; 

        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
        $pathInfo = pathinfo($currentPath); 

        // output: localhost
        $hostName = $_SERVER['HTTP_HOST']; 

        // output: http://
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

        // return: http://localhost/myproject/
        $base = $protocol.'://'.$hostName.$pathInfo['dirname']."/";

        try {

            $this->_client = new \Infusionsoft\Infusionsoft([
              'clientId'     => $this->config['clientId'],
              'clientSecret' => $this->config['clientSecret'],
              'redirectUri'  => $base . 'redirectinfusionsoft',
            ]);

            $global = \craft\elements\GlobalSet::find()->andWhere(['handle'=>'crmToken'])->one ();
            



            if ($global && $global->mainContent) {
                
                $token = ($global->mainContent);

                $token = unserialize($token);

                $this->_client->setToken ($token);

                $access = $this->_client->refreshAccessToken();

                $global->mainContent = serialize($access);
                Craft::$app->elements->saveElement($global);
                $this->_client->setToken ($access);
            } else {
                
                $access = $this->_client->requestAccessToken($this->config['code']);
                $global->mainContent = serialize($access);
                Craft::$app->elements->saveElement($global);
                $this->_client->setToken ($access);
            }
        } catch (Exception $e) {
           return $this->_client;
        }
        catch (InfusionsoftException $e) {
           
          return $this->_client;
        }  
        catch (TokenExpiredException $e) {

         return $this->_client;
        }  

       
        // $response = $this->_client->refreshAccessToken ();
        // $this->_client->setToken (($response));
        $this->_client->setDebug (true);
        return $this->_client;
        
    }

    public function auth () {
        // output: /myproject/index.php
        $currentPath = $_SERVER['PHP_SELF']; 

        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
        $pathInfo = pathinfo($currentPath); 

        // output: localhost
        $hostName = $_SERVER['HTTP_HOST']; 

        // output: http://
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

        // return: http://localhost/myproject/
        $base = $protocol.'://'.$hostName.$pathInfo['dirname']."/";


        $client = new \Infusionsoft\Infusionsoft([
          'clientId'     => $this->config['clientId'],
          'clientSecret' => $this->config['clientSecret'],
          'redirectUri'  => $base  . 'redirectinfusionsoft',
        ]);
       
        header ("Location: " . $client->getAuthorizationUrl ());
    }

    public function saveauth () {

        // output: /myproject/index.php
        $currentPath = $_SERVER['PHP_SELF']; 

        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
        $pathInfo = pathinfo($currentPath); 

        // output: localhost
        $hostName = $_SERVER['HTTP_HOST']; 

        // output: http://
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

        // return: http://localhost/myproject/
        $base = $protocol.'://'.$hostName.$pathInfo['dirname']."/";

        $client = new \Infusionsoft\Infusionsoft([
          'clientId'     => $this->config['clientId'],
          'clientSecret' => $this->config['clientSecret'],
          'redirectUri'  => $base . 'redirectinfusionsoft'
        ]);
        $access = $client->requestAccessToken($_GET['code']);
        $global = \craft\elements\GlobalSet::find()->andWhere(['handle'=>'crmToken'])->one ();
        $global->mainContent = serialize($access);
            Craft::$app->elements->saveElement($global);
            var_dump ($access);
            die ();

    }

    public function saveNewUser ($user) {

        try {

            $data = [];
            // $data['FirstName'] = $user->firstName;
            // $data['LastName'] = $user->lastName;
            $data['Email'] = $user->email;
            $data['_ConditionType'] = base64_encode($user->condition);
            $data['_HashedPassword'] = Craft::$app->getSecurity()->hashPassword($user->newPassword);
            $data['_PackCode'] = $user->packCode;
            $data['_Optin'] = $user->optIn ? true : false;

            $id = $this->client->contacts('xml')->addWithDupCheck ($data,'Email');

            if ($id) {
                $result = $this->client->emails('xml')->optIn ($user->email, "Website");
            }

            $this->callFunnel ('userRegistered', $id);

            https://kabicarestaging.lowpitch.com/patient-login
            return $result;

        } catch (Exception $e) {
          // var_dump ($e);
        }
    }

    public function doesEmailExist ($email, $idAllowed = null) {
      
        $result = $this->client->contacts('xml')->findByEmail ($email,['Id']);

        if ($result && !empty($result) && isset ($result[0]) && isset ($result[0]['Id']) && $result[0]['Id'] != $idAllowed) {
            return true;
        }
        return false;
    }

    public function findUserByEmail ($email) {
        $selectedFields = ['_HashedPassword','_PackCode','_ConditionType','Id','Email','_ResetURL','_ResetCode','_Optin'];


        $result = $this->client->contacts('xml')->findByEmail($email, $selectedFields);
        return $result;
    }

    public function updatePatientProfile ($user, $id) {
        $result = $this->client->contacts('xml')->update ($id,[
            'Email' => $user->email,
            '_ConditionType' => base64_encode($user->condition),
            '_Optin' => $user->optIn ? true : false
        ]);
        return $result;
    }

    public function findUserById ($id) {
        $selectedFields = ['_HashedPassword','_PackCode','_ConditionType','Id','Email','_ResetURL',
        '_ResetCode','_Optin'];
        try {
            $result = $this->client->contacts('xml')->load($id, $selectedFields);
         } catch (Exception $e) {

          return null;
        }  catch (InfusionsoftException $e) {

          return null;
        }  

        return $result;
    }

    public function updateFieldById ($id, $data) {
        return $this->client->contacts('xml')->update ($id, $data);
    }

    public function callFunnel ($name, $id) {
        $global = \craft\elements\GlobalSet::find()->andWhere(['handle'=>'crmToken'])->one ();

        if ($global) {
            $this->client->funnels('xml')->achieveGoal($global->faq_title, $name, $id);    
        }

        
    }

    public function optIn ($id) {

        $contact = $this->client->contacts()->find($id);
        // echo "<pre>";
        // var_dump ($contact);
        // $contact->email_opted_in =true;
        $contact->opt_in_reason = 'User opted in through website';
        unset($contact->tag_ids);
        // $contact->given_name = "Toby2";
        $result = $contact->save ();
        // var_dump ($result);
        // $result = $this->client->contacts()->id = $id;

        // die();
        // ->optIn ($email, "Website");
        // $result = $this->client->emails('xml')->optIn ($email, "Website");
        return true;
    }

    public function sendEmail ($id, $template) {
        $result = $this->client->emails('xml')->sendTemplate ([$id], $template);
        // $result = $this->client->emails('xml')->sendEmail ([29],"trevor@thinkemotive.com", "~Contact.Email~","","","Text","Password reset","","Hello!");
        return $result;
    }

    public function loadUser () {
        $client = $this->client;

    }
    
    
}
