<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\services;

use modules\crmusermodule\CrmUserModule;
use craft\services\Users as BaseUsers;
use Craft;
use craft\base\Component;
use modules\crmusermodule\elements\User;
use craft\elements\User as BaseUser;
use Infusionsoft\Http\HttpException;
use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;

/**
 * User Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
class Users extends BaseUsers
{

    public function getUserByUsernameOrEmail(string $usernameOrEmail)
    {

        // var_dump ("Perform lookup of");
        // var_dump ($usernameOrEmail);

        if (!$usernameOrEmail) {
            return null;
        }
        $infusionsoft = CrmUserModule::getInstance()->infusionsoft;

        try {
            $foundUser = $infusionsoft->findUserByEmail ($usernameOrEmail);
            if ($foundUser) {

                $user = new User;
                $foundUser = $foundUser[0];
                $user->id = $foundUser['Id'];
                // $user->firstName = $foundUser['FirstName'];
                // $user->lastName = $foundUser['LastName'];
                $user->email = $foundUser['Email'];
                $user->password = isset ($foundUser['_HashedPassword']) ? $foundUser['_HashedPassword'] : null;
                $user->resetURL = isset ($foundUser['_ResetCode']) ? $foundUser['_ResetCode'] : null;
                $user->resetCode = isset ($foundUser['_ResetCode']) ? $foundUser['_ResetCode'] : null;
                $user->condition = base64_decode($foundUser['_ConditionType']);
                $user->packCode = isset($foundUser['_PackCode']) ? $foundUser['_PackCode'] : "";
                $user->optIn = isset($foundUser['_Optin']) ? $foundUser['_Optin'] : "";
                return $user;
            }
            return null; 
        } catch (HttpException $error) {
            return null;
        }
        

        
    }

    public function sendPasswordResetEmail(BaseUser $user): bool
    {
        
        $securityService = Craft::$app->getSecurity();
        $unhashedCode = $securityService->generateRandomString(32);

        // Strip underscores so they don't get interpreted as italics markers in the Markdown parser
        $unhashedCode = str_replace('_', StringHelper::randomString(1), $unhashedCode);

        $generalConfig = Craft::$app->getConfig()->getGeneral();

        $action = 'reset-password';

        // $path = $generalConfig->actionTrigger . '/users/' . $action;
        $path = '/' . $action;
        $params = [
            'code' => $unhashedCode,
            'id' => $user->id
        ];

        $scheme = UrlHelper::getSchemeForTokenizedUrl();
        $siteId = Craft::$app->getSites()->getCurrentSite()->id;
      
        $url = UrlHelper::siteUrl($path, $params, $scheme, $siteId);

        $infusionsoft = CrmUserModule::getInstance()->infusionsoft;

        try {
            $result = $infusionsoft->updateFieldById ($user->id, ['_ResetURL' => $url, '_ResetCode' => $unhashedCode]);


            return Craft::$app->getMailer()
                ->composeFromKey('forgot_password', ['link' => $url])
                ->setTo($user)
                ->send();


            // if ($result) {
            //     $result = $infusionsoft->optIn ($user->id);
            //     // var_dump ($result);
            //     // die ();
            //     // $infusionsoft->callFunnel ("passwordReset", $user->id);
            //     $result = $infusionsoft->sendEmail ( $user->id, 167);
            //     // var_dump ($result);
            //     // die ();

            //     return true;

            // }
            
        } catch (HttpException $error) {
            var_dump ("Something went wrong");
            echo "<pre>";
            var_dump ($error);
        }

        return false;

      
    }

    public function isVerificationCodeValidForUser(BaseUser $user, string $code): bool
    {

        if (isset ($user->resetCode)) {
            return $user->resetCode == $code;    
        }

        

        return parent::isVerificationCodeValidForUser($user, $code);

        
        
        // return true;
    }
   


    public function getUserByUid (string $uid) {
        return $this->getUserById ($uid);
    }


    public function getUserById (int $userId) 
    {


        $session = Craft::$app->getSession();
        $foundUser =$session->get('user');
        $foundUser = null;
        if (!$foundUser) {
            $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
            try {
                $foundUser = $infusionsoft->findUserById ($userId);
            } catch (HttpException $error) {
                return null;
            }
            if ($foundUser) {
                // $session->set('user', $foundUser);
            } else {
                return null;
            }
            
        } 

        
        if ($foundUser) {

            $user = new User;
            $user->id = $foundUser['Id'];

            // $user->firstName = $foundUser['FirstName'];
            // $user->lastName = $foundUser['LastName'];
            $user->email = $foundUser['Email'];
            $user->password = $foundUser['_HashedPassword'];
            $user->resetURL = isset ($foundUser['_ResetURL']) ? $foundUser['_ResetURL'] : null;
            $user->resetCode = isset ($foundUser['_ResetCode']) ? $foundUser['_ResetCode'] : null;
            $user->condition = base64_decode($foundUser['_ConditionType']);
            $user->packCode = isset($foundUser['_PackCode']) ? $foundUser['_PackCode'] : "";
            $user->optIn = isset($foundUser['_Optin']) ? $foundUser['_Optin'] : "";
            return $user;
        }
        return null;
    }

    public function handleValidLogin(BaseUser $user)
    {
        
    }




    public function saveUserPreferences(BaseUser $user, array $preferences)
    {
        // do absolutely nothing here...
    }

    public function assignUserToDefaultGroup(BaseUser $user): bool
    {
        // do absolutely nothing here...

        return true;
    }


    
}
