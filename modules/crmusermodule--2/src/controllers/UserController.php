<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\controllers;

use modules\crmusermodule\CrmUserModule;
use modules\crmusermodule\elements\User;

use Craft;
use craft\controllers\UsersController as BaseController;
use yii\web\HttpException;
use craft\helpers\UrlHelper;




/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
class UserController extends BaseController
{

    public static function checkHCP () {

        $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();
        if (!$generic->restrictionEnabled) {
            return;
        }
         $session = Craft::$app->getSession();
         $hcpValue = $session->get('hcp_status');
         if (!$hcpValue) {
            $userSession = Craft::$app->getUser();
            $userSession->loginRequired();
            Craft::$app->end();
         }
    }

    public static function checkPatient () {

        $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();
        if (!$generic->restrictionEnabled) {
            return;
        }
        
        $userSession = Craft::$app->getUser();

        if ($userSession->getIsGuest()) {
            $userSession->loginRequired();
            Craft::$app->end();
        }
    }

    // public function requireLogin()
    // {

    //     var_dump ("Die");
    //     die ();
    //     $userSession = Craft::$app->getUser();

    //     if ($userSession->getIsGuest()) {
    //         var_dump ("1");
    //         die ();
    //         $userSession->loginRequired();
    //         Craft::$app->end();
    //     }
    // }



    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['login','register','auth','reset','isauth','saveauth','hcp-return'];

    public function actionAuth () 
    {
        $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
        $infusionsoft->auth ();
        die ();
    }

    public function actionSaveauth () {
        $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
        $infusionsoft->saveauth ();
        die ();
    }

    /**
     * @return array|Response
     */
    private function _processTokenRequest()
    {
        $uid = Craft::$app->getRequest()->getRequiredParam('id');
        $code = Craft::$app->getRequest()->getRequiredParam('code');
        $isCodeValid = false;

        /** @var User|null $userToProcess */
        $userToProcess = User::find()
            ->uid($uid)
            ->anyStatus()
            ->addSelect(['users.password', 'users.unverifiedEmail'])
            ->one();

        // If someone is logged in and it's not this person, log them out
        $userSession = Craft::$app->getUser();
        if (($identity = $userSession->getIdentity()) !== null && $userToProcess && $identity->id != $userToProcess->id) {
            $userSession->logout();
        }

        if ($userToProcess) {
            // Fire a 'beforeVerifyUser' event
            Craft::$app->getUsers()->trigger(Users::EVENT_BEFORE_VERIFY_EMAIL,
                new UserEvent([
                    'user' => $userToProcess
                ]));

            $isCodeValid = Craft::$app->getUsers()->isVerificationCodeValidForUser($userToProcess, $code);
        }

        if (!$userToProcess || !$isCodeValid) {
            return $this->_processInvalidToken();
        }

        // Fire an 'afterVerifyUser' event
        Craft::$app->getUsers()->trigger(Users::EVENT_AFTER_VERIFY_EMAIL,
            new UserEvent([
                'user' => $userToProcess
            ]));

        return [$userToProcess, $uid, $code];
    }

    private function _handleInvalidToken () {
        if ($url = Craft::$app->getConfig()->getGeneral()->getInvalidUserTokenPath()) {
            return $this->redirect(UrlHelper::siteUrl($url));
        }
        throw new HttpException('200', Craft::t('app', 'Invalid verification code. Please login or reset your password.'));
        die ();
    }


    public function actionIsauth () {
         $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
        $infusionsoft->auth ();
        die ();
    }



    public function actionReset () {

        $code = Craft::$app->getRequest()->getRequiredParam('code');
        $uid = Craft::$app->getRequest()->getRequiredParam('id');
        $userToProcess = Craft::$app->getUsers()->getUserByUid($uid);

      
        if (!Craft::$app->getRequest()->getIsPost()) {
           
           if ($userToProcess->resetCode != $code) {
            return $this->_handleInvalidToken ();
           }
          

            Craft::$app->getUser()->sendUsernameCookie($userToProcess);

            // Send them to the set password template.
            return $this->renderTemplate('reset', [
                'code' => $code,
                'id' => $uid
            ]);
        }

        // POST request. They've just set the password.
        

        // See if we still have a valid token.
        $isCodeValid = Craft::$app->getUsers()->isVerificationCodeValidForUser($userToProcess, $code);

        if (!$userToProcess || !$isCodeValid) {
            return $this->_handleInvalidToken ();
        }

        $userToProcess->newPassword = Craft::$app->getRequest()->getRequiredBodyParam('newPassword');
       

        if ($userToProcess->validate ()) {
            $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
            $result = $infusionsoft->updateFieldById ($userToProcess->id, ['_HashedPassword' => Craft::$app->getSecurity()->hashPassword($userToProcess->newPassword),'_ResetURL' => 'null', '_ResetCode' => 'null']);

            $setPasswordSuccessPath = Craft::$app->getConfig()->getGeneral()->getSetPasswordSuccessPath();
            $url = UrlHelper::siteUrl($setPasswordSuccessPath);
            return $this->redirect($url);
        }

        Craft::$app->getSession()->setError(Craft::t('app', 'Couldn’t update password.'));

        $errors = $userToProcess->getErrors('newPassword');

        return $this->renderTemplate('reset', [
            'code' => $code,
            'id' => $uid,
            'errors' => $errors,
        ]);

    }

    public function actionLogin()


    {

       
        if (!Craft::$app->getUser()->getIsGuest()) {
            // Too easy.
            return $this->_handleSuccessfulLogin(false);
        }

        if (!Craft::$app->getRequest()->getIsPost()) {
            $this->_enforceOfflineLoginPage();
            return null;
        }

        $loginName = Craft::$app->getRequest()->getRequiredBodyParam('loginName');
        $password = Craft::$app->getRequest()->getRequiredBodyParam('password');
        $rememberMe = (bool)Craft::$app->getRequest()->getBodyParam('rememberMe');

        // var_dump (Craft::$app->getUsers());

        // Does a user exist with that username/email?
        $user = Craft::$app->getUsers()->getUserByUsernameOrEmail($loginName);

        // Delay randomly between 0 and 1.5 seconds.
        usleep(random_int(0, 1500000));

        if (!$user || $user->password === null) {
            // Delay again to match $user->authenticate()'s delay
            Craft::$app->getSecurity()->validatePassword('p@ss1w0rd', '$2y$13$nj9aiBeb7RfEfYP3Cum6Revyu14QelGGxwcnFUKXIrQUitSodEPRi');
            return $this->_handleLoginFailure(User::AUTH_INVALID_CREDENTIALS);
        }

        // Did they submit a valid password, and is the user capable of being logged-in?
        if (!$user->authenticate($password)) {
            return $this->_handleLoginFailure($user->authError, $user);
        }


        // Get the session duration
        $generalConfig = Craft::$app->getConfig()->getGeneral();
        if ($rememberMe && $generalConfig->rememberedUserSessionDuration !== 0) {
            $duration = $generalConfig->rememberedUserSessionDuration;
        } else {
            $duration = $generalConfig->userSessionDuration;
        }


        // Try logging them in
        if (!Craft::$app->getUser()->login($user, $duration)) {

            // Unknown error
            return $this->_handleLoginFailure(null, $user);
        }

        return $this->_handleSuccessfulLogin(true);
    }


    private function _handleSuccessfulLogin(bool $setNotice)
    {
        // Get the return URL
        $userSession = Craft::$app->getUser();


          $entry = \craft\elements\Entry::find()
                ->id(213)
                ->one();

                $returnUrl = '/' .$entry->uri;

        // Clear it out
        $userSession->removeReturnUrl();

        // If this was an Ajax request, just return success:true
        if (Craft::$app->getRequest()->getAcceptsJson()) {
            return $this->asJson([
                'success' => true,
                'returnUrl' => $returnUrl
            ]);
        }

        if ($setNotice) {
            Craft::$app->getSession()->setNotice(Craft::t('app', 'Logged in.'));
        }

        return $this->redirectToPostedUrl($userSession->getIdentity(), $returnUrl);
    }



   
    public function actionRegister()
    {


       
        // $this->requirePostRequest();

        $request = Craft::$app->getRequest();
        $userSession = Craft::$app->getUser();
        $currentUser = $userSession->getIdentity();
        
        $user = new User();
        $user->scenario = 'create';

        $newEmail = $request->getBodyParam('email');
        $user->email = $newEmail;
        $user->newPassword = $request->getBodyParam('password', '');
        $user->confirmPassword = $request->getBodyParam('confirmPassword', '');
        $user->condition = $request->getBodyParam('condition', $user->condition);
        $user->optIn = $request->getBodyParam('opt-in', $user->optIn);
        // $user->firstName = $request->getBodyParam('firstName', $user->firstName);
        // $user->lastName = $request->getBodyParam('lastName', $user->lastName);
        $user->packCode = $request->getBodyParam('packCode', $user->packCode);

         
      
        // Manually validate the user so we can pass $clearErrors=false
        if (
            !$user->validate(null, false)             
        ) {
            Craft::info('User not saved due to validation error.', __METHOD__);

           
            // Move any 'newPassword' errors over to 'password'
            $user->addErrors(['password' => $user->getErrors('newPassword')]);
            $user->clearErrors('newPassword');

            Craft::$app->getSession()->setError(Craft::t('app', 'Couldn’t save user.'));

            if (Craft::$app->getRequest()->getAcceptsJson()) {
                return $this->asJson([
                   
                    'errors' => $user->getErrors ()
                ]);
            }

            // Send the account back to the template
            Craft::$app->getUrlManager()->setRouteParams([
                'user' => $user
            ]);
          
            return null;
        } else {
            $infusionsoft = CrmUserModule::getInstance()->infusionsoft;

            // see if the email exists...
            $doesEmailExist = $infusionsoft->doesEmailExist ($user->email);

            if ($doesEmailExist) {
                $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();
                $user->addError ('email',$generic->emailexists);

                if (Craft::$app->getRequest()->getAcceptsJson()) {
                    return $this->asJson([
                       
                        'errors' => $user->getErrors ()
                    ]);
                }

                 Craft::$app->getUrlManager()->setRouteParams([
                    'user' => $user
                ]);

                 return null;
            } else {
                $newId = $infusionsoft->saveNewUser ($user);  
                $generalConfig = Craft::$app->getConfig()->getGeneral();
                $user->id = $newId;
               
                $usedPackCodes =  \craft\elements\GlobalSet::find()->andWhere(['handle'=>'alreadyUsedPackCodes'])->one ();
                if (!$usedPackCodes->packCodes) {
                    $usedPackCodes->packCodes = "";
                }
                $usedPackCodes->packCodes .= "\n" . $user->packCode;
                
               Craft::$app->elements->saveElement($usedPackCodes);

                $user = Craft::$app->getUsers()->getUserByUsernameOrEmail($newEmail);


                 $result = Craft::$app->getUser()->login($user, $generalConfig->userSessionDuration);


                // var_dump ($user);
               
                // Craft::$app->getUser()->login($user, $duration);


                // var_dump ($user);
              
            } 
        }

         
        if (Craft::$app->getRequest()->getAcceptsJson()) {
            

            $entry = \craft\elements\Entry::find()
                ->id(213)
                ->one();

            return $this->asJson([
                'success' => true,
                'returnUrl' => '/' . $entry->uri // '/patient/home'//$returnUrl
            ]);
        }

        return $this->redirectToPostedUrl($user);
    }

    public function actionHcpReturn () {
         $slug = craft\elements\GlobalSet::find()->andWhere(['handle'=>'hcpSlug'])->one ();
                
         $session = Craft::$app->getSession();

         $entry = \craft\elements\Entry::find()
                ->id(3226)
                ->one();
         $hcpValue = $session->set('hcp_status', 1);
         return $this->redirect('/' . $entry->uri . "?hcp-access=1");
    }

    public function actionPatientProfile () {
        
        $this->requirePostRequest();

        $request = Craft::$app->getRequest();
        $userSession = Craft::$app->getUser();

        $currentUser = $userSession->getIdentity();
       

         $user = new User();
        
        // Handle secure properties (email and password)
        // ---------------------------------------------------------------------

        $verifyNewEmail = false;

      
        $newEmail = $request->getBodyParam('email');
        $user->email = $newEmail;
        $user->optIn =  $request->getBodyParam('opt-in');
        $user->scenario='patientProfile';
        $user->condition = $request->getBodyParam('condition');

         // Manually validate the user so we can pass $clearErrors=false
        if (
            !$user->validate(null, false)             
        ) {
            Craft::info('User not saved due to validation error.', __METHOD__);

           
            // Move any 'newPassword' errors over to 'password'
            $user->addErrors(['password' => $user->getErrors('newPassword')]);
            $user->clearErrors('newPassword');
            $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();
            Craft::$app->getSession()->setError(Craft::t('app', $generic->couldntUpdateUser));

            if (Craft::$app->getRequest()->getAcceptsJson()) {
                return $this->asJson([
                   
                    'errors' => $user->getErrors ()
                ]);
            }

            // Send the account back to the template
            Craft::$app->getUrlManager()->setRouteParams([
                'user' => $user
            ]);
          
            return null;
        }
         else {
            $infusionsoft = CrmUserModule::getInstance()->infusionsoft;
           
            // see if the email exists...
            $doesEmailExist = $infusionsoft->doesEmailExist ($user->email, $currentUser->id);

            if ($doesEmailExist) {
                $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();

                $user->addError ('email',$generic->emailexists);

                if (Craft::$app->getRequest()->getAcceptsJson()) {
                    return $this->asJson([
                       
                        'errors' => $user->getErrors ()
                    ]);
                }

                 Craft::$app->getUrlManager()->setRouteParams([
                    'user' => $user
                ]);

                 return null;
            } else {
                $result = $infusionsoft->updatePatientProfile ($user, $currentUser->id);    

            } 
        }

        if (Craft::$app->getRequest()->getAcceptsJson()) {
            return $this->asJson([
                'success' => true,
                'returnUrl' => ''//$returnUrl
            ]);
        }


        
        



        Craft::$app->getSession()->setFlash("notice","Your profile has been updated");
      

        return $this->redirectToPostedUrl($user);


        
    }



    

}
