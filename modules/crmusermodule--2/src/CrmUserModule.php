<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule;

use modules\crmusermodule\assetbundles\crmusermodule\CrmUserModuleAsset;
use modules\crmusermodule\services\User as UserService;
use modules\crmusermodule\variables\CrmUserModuleVariable;
use modules\crmusermodule\elements\User as UserElement;
use modules\crmusermodule\twigextensions\CrmUserModuleTwigExtension;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\services\Assets;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

use craft\events\ElementEvent;
use craft\events\ReplaceAssetEvent;
use craft\elements\Entry;
use craft\elements\Asset;


/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 *
 * @property  UserService $user
 */
class CrmUserModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * CrmUserModule::$instance
     *
     * @var CrmUserModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    public $config;

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/crmusermodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\crmusermodule\controllers';


        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/crmusermodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * CrmUserModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        // Load our AssetBundle
        if (Craft::$app->getRequest()->getIsCpRequest()) {
            Event::on(
                View::class,
                View::EVENT_BEFORE_RENDER_TEMPLATE,
                function (TemplateEvent $event) {
                    try {
                        Craft::$app->getView()->registerAssetBundle(CrmUserModuleAsset::class);
                    } catch (InvalidConfigException $e) {
                        Craft::error(
                            'Error registering AssetBundle - '.$e->getMessage(),
                            __METHOD__
                        );
                    }
                }
            );

            Event::on(
                Elements::class, 
                Elements::EVENT_BEFORE_SAVE_ELEMENT, 
                function(ElementEvent $event) {
                    if ($event->element instanceof Entry) {
                        $entry = $event->element;
                        if (isset ($entry->modules)) {
                            foreach ($entry->modules as $module) {

                                $moduleContent = $module->module_content->one();
                                if ($moduleContent) {

                                    if (strpos(strtolower($moduleContent->filename),".zip") !== FALSE) {
                                        $path = CRAFT_BASE_PATH . '/web' . $moduleContent->url;
                                        $unzipPath = CRAFT_BASE_PATH . '/web/unzip' . $moduleContent->url;
                                        if (file_exists ($path) && !file_exists($unzipPath)) {
                                            $zip = new \ZipArchive;
                                            if ($zip->open($path) === TRUE) {
                                                $zip->extractTo($unzipPath);
                                                $zip->close();
                                            }
                                        }
                                      
                                    }
                                    

                                  
                                }
                                 

                                
                            }
                            

                        }

                       


                        // if($event->isNew){
                        //     $entry->reference= 'AV' . (string)$entry->id;
                        //     Craft::$app->elements->saveElement($entry);    
                        // }
                    }
            }); 


            // Event::on(
            //     Assets::class, 
            //     Assets::EVENT_BEFORE_REPLACE_ASSET, 
            //     function( $event) {
            //         var_dump ($event);
            //         die ();
                
            // });

        }

        // // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['reset-password'] = 'crm-user-module/user/reset';
            }
        );

         Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['infusionsoftauth'] = 'crm-user-module/user/isauth';
            }
        );

          Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['redirectinfusionsoft'] = 'crm-user-module/user/saveauth';
            }
        );


        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['patient-profile'] = 'crm-user-module/user/patient-profile';
            }
        );


        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['hcp/return'] = 'crm-user-module/user/hcp-return';
            }
        );

       

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = UserElement::class;
            }
        );

      

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('crmUserModule', CrmUserModuleVariable::class);
            }
        );

        Craft::$app->view->registerTwigExtension(new CrmUserModuleTwigExtension());
        $session = Craft::$app->getSession();
        $hcpValue = $session->get('hcp_status');
        $twig = Craft::$app->view->getTwig(null, ['safe_mode' => false]);
        $twig->addGlobal('hcpUser', $hcpValue ? true : false);
/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'crm-user-module',
                '{name} module loaded',
                ['name' => 'CrmUser']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
