<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */
namespace modules\crmusermodule\twigextensions;

use Craft;
use modules\crmusermodule\controllers\UserController;

/**
 * Class RequireLoginNode
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.0
 */
class RequirePatientNode extends \Twig_Node
{
    // Public Methods
    // =========================================================================

    /**
     * Compiles a RequireLoginNode into PHP.
     *
     * @param \Twig_Compiler $compiler
     */
    public function compile(\Twig_Compiler $compiler)
    {


        $compiler
            ->addDebugInfo($this)
            ->write(UserController::class . "::checkPatient();\n");
    }
}
