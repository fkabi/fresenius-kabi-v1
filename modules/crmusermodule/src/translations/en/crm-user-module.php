<?php
/**
 * CrmUser module for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

/**
 * CrmUser en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('crm-user-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    3 Feet High
 * @package   CrmUserModule
 * @since     1.0.0
 */
return [
    'CrmUser plugin loaded' => 'CrmUser plugin loaded',
];
