<?php
/**
 * CrmUser plugin for Craft CMS 3.x
 *
 * Module to handle authentication and data storage via CRM sync
 *
 * @link      lowpitch.com
 * @copyright Copyright (c) 2019 3 Feet High
 */

namespace modules\crmusermodule\elements;

use modules\crmusermodule\CrmUserModule;

use Craft;
use craft\elements\User as BaseUser;
use craft\validators\UserPasswordValidator;
use craft\events\AuthenticateUserEvent;

class User extends BaseUser
{

    public $condition;
    public $packCode;
    public $resetURL;
    public $resetCode;
    public $optIn;
    public $confirmPassword;
    public $provideFeedback;
    
    public function rules()
    {


        $generic = \craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();


        
        $rules[] = [['email'], 'email' ,'message'=>$generic->emailAddressInvalid];
        $rules[] = [['email'], 'string', 'max' => 255,'message'=>$generic->emailAddressInvalid];
        $rules[] = [[ 'newPassword'], 'string', 'max' => 255,'message'=>$generic->passwordInvalid];
        $rules[] = [['username', 'firstName', 'lastName','confirmPassword'], 'string', 'max' => 100];
        $rules[] = [['confirmPassword', 'condition','newPassword','email','packCode'], 'required','on'=>'create','message'=>$generic->requiredFieldsMissing];
        $rules[] = [['condition','email'], 'required','on'=>'patientProfile'];
        $rules[] = [['packCode'], 'validatePackCode','skipOnEmpty'=>false,'on'=>'create'];
        $rules[] = [['confirmPassword'], 'validateConfirm','skipOnEmpty'=>false,'on'=>'create'];
       
        $rules[] =

        $rules[] = [
            ['newPassword'],
            UserPasswordValidator::class,
            'message'=>$generic->passwordInvalid,
            'tooShort'=>$generic->passwordInvalid,
        ];



        // $rules[] = [['firstName', 'lastName'], function ($attribute, $params, $validator) {
        //     if (strpos($this->$attribute, '://') !== false) {
        //         $validator->addError($this, $attribute, Craft::t('app', 'Invalid value “{value}”.'));
        //     }
        // }];

        return $rules;
    }

    public function validatePackCode($attribute, $params, $validator)
    {
       
        $packCodes = $global = \craft\elements\GlobalSet::find()->andWhere(['handle'=>'packCodes'])->one ();

        $usedPackCodes =  \craft\elements\GlobalSet::find()->andWhere(['handle'=>'alreadyUsedPackCodes'])->one ();

        if ($packCodes) {
            $packCodes = strtolower ($packCodes->packCodes);
            $packCodes = str_replace("\r","", $packCodes);
            $packCodes = explode("\n",$packCodes);
            if (!$usedPackCodes) {
                $usedPackCodes = array ();
            } else {
                $usedPackCodes = strtolower($usedPackCodes->packCodes);
                $usedPackCodes = str_replace("\r","", $usedPackCodes);
                $usedPackCodes = explode("\n",$usedPackCodes);
    
            }
            
            if (!in_array(strtolower($this->$attribute), $packCodes)) {
                $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();

                $this->addError($attribute, $generic->invalidPackCode);
            } else if (in_array(strtolower($this->$attribute), $usedPackCodes)) {
                $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();

                $this->addError($attribute, $generic->packCodeUsed);
            }
        }
    }

    public function validateConfirm($attribute, $params, $validator)
    {

        if ($this->newPassword != $this->confirmPassword) {
            $generic = craft\elements\GlobalSet::find()->andWhere(['handle'=>'genericContent'])->one ();
            $this->addError ($attribute, $generic->passwordsDoNotMatch);
        }
    }

    public static function findIdentity($id)
    {
        
        
        return CrmUserModule::getInstance()->users->getUserById ($id);
        
    }

    public function can(string $permission): bool
    {
        if (Craft::$app->getEdition() === Craft::Pro) {
            if ($this->admin) {
                return true;
            }

           
            
            // if ($this->id !== null) {
            //     return Craft::$app->getUserPermissions()->doesUserHavePermission($this->id, $permission);
            // }

            return false;
        }

        return true;
    }



    public function authenticate(string $password): bool
    {
        // Fire a 'beforeAuthenticate' event
        $event = new AuthenticateUserEvent([
            'password' => $password,
        ]);
        $this->trigger(self::EVENT_BEFORE_AUTHENTICATE, $event);

        if ($event->performAuthentication) {
            // Validate the password
            try {
                if (function_exists('password_verify')) {
                    $passwordValid = password_verify($password, $this->password);
                } else {
                    $passwordValid = $this->password == Craft::$app->getSecurity()->hashPassword($password);    
                } 
            } catch (InvalidArgumentException $e) {
                $passwordValid = false;
            }
           
            if ($passwordValid) {
                
                $this->authError = null;
            } else {
                $this->authError = self::AUTH_INVALID_CREDENTIALS;
            }
        }

        return $this->authError === null;
    }

}
