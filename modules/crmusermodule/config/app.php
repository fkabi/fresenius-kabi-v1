<?php
/**
 * Yii Application Config
 *
 * Edit this file at your own risk!
 *
 * The array returned by this file will get merged with
 * vendor/craftcms/cms/src/config/app/main.php and [web|console].php, when
 * Craft's bootstrap script is defining the configuration for the entire
 * application.
 *
 * You can define custom modules and system components, and even override the
 * built-in system components.
 */
return [
    'modules' => [
        'crm-user-module' => [
            'class' => \modules\crmusermodule\CrmUserModule::class,
            'components' => [
                'user' => [
                    'class' => 'modules\crmusermodule\services\User',

                ],
                'infusionsoft' => [
                    'class' => 'modules\crmusermodule\services\Infusionsoft',
                    'config' => [
                      'clientId' => 'aa3n49jcwenkk2sqqqjy72vy',
                      'clientSecret' => '3uZbw7jPqu',
                      'redirectUri' => ''
                    ]
                ],
            ],
        ],
    ],
    'bootstrap' => ['crm-user-module'],
];
