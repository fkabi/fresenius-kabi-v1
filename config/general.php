<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        'userSessionDuration' => 600,
        'invalidUserTokenPath' => '/forgotpassword?invalidtoken',

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        'rememberedUserSessionDuration' => 0,

        'maxUploadFileSize'     => 1073741824,

        'autoLoginAfterAccountActivation' => true,

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => true,

        'allowUpdates' => true,

        'loginPath' => function ($siteHandle) {
            $slug = craft\elements\GlobalSet::find()->andWhere(['handle'=>'hcpSlug'])->one ();
           
            if (substr(Craft::$app->request->getPathInfo(), 0,strlen($slug->hcpSlug)) === $slug->hcpSlug || substr(Craft::$app->request->getPathInfo(), 0,3) === 'hcp') {
                return $slug->hcpSlug . '/?requireLogin';
            }
            // return 'patient/?requireLogin';
            return $slug->patientslug . '/?requireLogin';
        }
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => true,
    ],

    // Staging environment settings
    'staging' => [
        // Prevent administrative changes from being made on staging
        'allowAdminChanges' => false,
    ],

    // Production environment settings
    'production' => [
        // Prevent administrative changes from being made on production
        'allowAdminChanges' => false,
    ],
];
