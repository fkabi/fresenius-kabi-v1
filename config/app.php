<?php
/**
 * Yii Application Config
 *
 * Edit this file at your own risk!
 *
 * The array returned by this file will get merged with
 * vendor/craftcms/cms/src/config/app.php and app.[web|console].php, when
 * Craft's bootstrap script is defining the configuration for the entire
 * application.
 *
 * You can define custom modules and system components, and even override the
 * built-in system components.
 *
 * If you want to modify the application config for *only* web requests or
 * *only* console requests, create an app.web.php or app.console.php file in
 * your config/ folder, alongside this one.
 */

return [
    'modules' => [
        'crm-user-module' => [
            'class' => \modules\crmusermodule\CrmUserModule::class,
            'components' => [
                'user' => [
                    'class' => 'modules\crmusermodule\services\User',
                ],
                'infusionsoft' => [
                    'class' => 'modules\crmusermodule\services\Infusionsoft',
                    'config' => [
                      'clientId' => 'XXXXXXX',
                      'clientSecret' => 'XXXXXXX',
                      'code' => '',
                      'redirectUri' => ''
                    ]
                ],
            ],
        ],
    ],
    'bootstrap' => ['crm-user-module'],
    //'bootstrap' => ['my-module'],
    //
    
    'components' => [
      'mailer' => function() {


            // Get the stored email settings
            $settings = craft\helpers\App::mailSettings();

            $mailerSettings = craft\elements\GlobalSet::find()->andWhere(['handle'=>'mailerSettings'])->one ();

            if ($mailerSettings && $mailerSettings->fromEmail) {
              $settings->fromEmail = $mailerSettings->fromEmail;
            }
            if ($mailerSettings->fromName) {
              $settings->fromName = $mailerSettings->fromName;
              
            }
            if ($mailerSettings->postmarkToken) {
              $settings->transportSettings['token'] = $mailerSettings->postmarkToken;
            }

            // Create a Mailer component config with these settings
            $config = craft\helpers\App::mailerConfig($settings);
           
            // Instantiate and return it
            return Craft::createObject($config);
        },

    ]
];
